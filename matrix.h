// A matrix implementation for efficiently applying a [-1, 0, 1] derivative
// filter on a matrix, over both axises. It uses a two-level heirarchy,
// "blocks" of row-major integers, which are in turn layed out in row-major
// to form the matrix.

// The goal of this nonlinear layout is make maximum use of caches.

// Author: Akihiro Matsukawa

// The blocks. They are typicall square, except for edges of the matrix.
// Each block is linked to its right and down neighbor, for easy access when
// applying the filter.
// The "data" pointer of the block points to parts of the data array for
// a matrix. Essentially, the blocks fork into different parts of the matrix's
// array that marks the start of each block.
typedef struct block Block;
struct block {
    int width;
    int height;
    int* data;
    Block *right, *down;
};

// The matrix. It has the usual parameters, plus blocks. The blocks are layed
// out in row major as well (a matrix of blocks, as it were).
// blockSize is the size used. The optimal value here is machine dependent.
// On a Macbook Pro 15" Retina, values of about 200 seemed to do it.
// That seems rather large, so on older archetectures with less cache,
// a value like 64 maybe more appropriate (64x64 => 4KB blocks).
typedef struct matrix Matrix;
struct matrix {
    int width;
    int height;
    int blockSize;
    int blockWidth;
    int blockHeight;
    int* data;
    Block* blocks;
};

// deallocate a matrix
void freeMatrix(Matrix* m);

// fill the matrix with random data, and call setupBlocks
void fillMatrix(Matrix* m);

// configures the block structure.
void setupBlocks(Matrix* m);

// prints a matrix.
// * not really written efficiently, but good enough
void printMatrix(Matrix* m);

// get the element at coordinates (x,y). mostly used by printMatrix
int getElement(Matrix* m, int x, int y);

// allocate a matrix, eg. the matrix struct, its content array, and block
// array.
Matrix* allocateMatrix(int width, int height, int blockSize);
