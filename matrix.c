#include <stdlib.h>
#include <stdio.h>
#include "matrix.h"

void setupBlocks(Matrix* m) {
    int* currentInt = m->data;
    Block* currentBlock = m->blocks;
    for (int y = 0; y < m->blockHeight; y++) {
        for (int x = 0; x < m->blockWidth; x++) {
            currentBlock->data = currentInt;
            if (y < m->blockHeight - 1 && x < m->blockWidth - 1) {
                currentBlock->width = currentBlock->height = m->blockSize;
                currentBlock->right = currentBlock + 1;
                currentBlock->down = currentBlock + m->blockWidth;
            }
            else if (y < m->blockHeight - 1) {
                int remainder = m->width % m->blockSize;
                currentBlock->width = remainder == 0 ? m->blockSize : remainder;
                currentBlock->height = m->blockSize;
                currentBlock->right = NULL;
                currentBlock->down = currentBlock + m->blockWidth;
            }
            else if (x < m->blockWidth - 1) {
                int remainder = m->height % m->blockSize;
                currentBlock->height = remainder == 0 ? m->blockSize : remainder;
                currentBlock->width = m->blockSize;
                currentBlock->right = currentBlock + 1;
                currentBlock->down = NULL;
            }
            else {
                int remainder = m->width % m->blockSize;
                currentBlock->width = remainder == 0 ? m->blockSize : remainder;
                remainder = m->height % m->blockSize;
                currentBlock->height = remainder == 0 ? m->blockSize : remainder;
                currentBlock->right = NULL;
                currentBlock->down = NULL;
            }
            currentInt = currentInt + currentBlock->width * currentBlock->height;
            currentBlock++;
        }
    }
}

Matrix* allocateMatrix(int width, int height, int blockSize) {
    Matrix* m = (Matrix*) malloc(sizeof(Matrix));
    m->width = width;
    m->height = height;
    m->blockWidth = width / blockSize;
    m->blockHeight = height / blockSize;
    if (width % blockSize != 0)
        m->blockWidth += 1;
    if (height % blockSize != 0)
        m->blockHeight += 1;
    m->blockSize = blockSize;
    m->data = (int*) malloc(sizeof(int)*width*height);
    m->blocks = (Block*) malloc(sizeof(Block) * m->blockWidth * m->blockHeight);
    setupBlocks(m);
    return m;
}

void freeMatrix(Matrix* m) {
    free(m->data);
    free(m->blocks);
    free(m);
}

void fillMatrix(Matrix* m) {
    int length = m->width * m->height;
    int* current = m->data;
    for (int i = 0; i < length; i++) {
        *current = rand();
        current++;
    }
}

// precondition: 0 <= x < m->width, = <= y < m->height
int getElement(Matrix* m, int x, int y) {
    int xBlock = x / m->blockSize;
    int yBlock = y / m->blockSize;
    int blockWidth;
    int blockHeight;
    if (xBlock < m->width / m->blockSize) {
        blockWidth = m->blockSize;
    } else {
        blockWidth = m->width % m->blockSize;
    }
    if (yBlock < m->height/m->blockSize) {
        blockHeight = m->blockSize;
    } else {
        blockHeight = m->height % m->blockSize;
    }
    int* corner = m->data + yBlock * m->blockSize
        * m->width + xBlock * m->blockSize * blockHeight;
    return *(corner + blockWidth*(y % m->blockSize) + (x % m->blockSize));
}

void printMatrix(Matrix* m) {
    for (int y = 0; y < m->height; y++) {
        for (int x = 0; x < m->width; x++) {
            printf("%d ", getElement(m, x, y));
        }
        printf("\n");
    }
    printf("\n");
}