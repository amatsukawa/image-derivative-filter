import sys
import numpy as np
from scipy.ndimage.filters import convolve


def read_matrix(path):
    mtx = []
    f = open(path)
    line = f.readline().strip()
    line = f.readline().strip()
    while line:
        numbers = [int(x) for x in line.split(" ")]
        mtx.append(numbers)
        line = f.readline().strip()
    f.close()
    return np.array(mtx)


def apply_filter(mtx, filter):
    return convolve(mtx, filter, mode='constant', cval=0)


def print_mtx(mtx):
    for row in mtx:
        row_text = ""
        for elem in row:
            row_text += str(elem) + " "
        print row_text

if __name__ == "__main__":
    mtx = read_matrix(sys.argv[1])
    print "Mtx"
    print_mtx(mtx)
    print "\nDx"
    dx = apply_filter(mtx, [[-1, 0, 1]])
    dx_flatten = dx.flatten()
    print_mtx(dx)
    print "\nDy"
    dy = apply_filter(mtx, [[-1], [0], [1]])
    dy_flatten = dy.flatten()
    print_mtx(dy)
    print "\n(min, max): dx=(%s, %s), dy=(%s, %s)" % \
        (min(dx_flatten), max(dx_flatten), min(dy_flatten), max(dy_flatten))
