default: main

main: filter filter_old

filter:
	gcc -Wall -O3 -c -std=c99 filter.c matrix.c
	gcc -Wall -O3 filter.o matrix.o -o filter

filter_old:
	gcc -Wall -c -std=c99 -O3 filter_old.c
	gcc -Wall filter_old.o -o filter_old

clean:
	rm *.o
	rm filter filter_old

check: main
	# a heuristic test, uses python to validate that the answers appear
	# to be correct for a few inputs. no output from diff means good to go.
	./filter 10 8 | sed '$$d' > test
	python filter.py test > test2
	diff test test2
	rm test test2
	./filter 1000 100 | sed '$$d' > test
	python filter.py test > test2
	diff test test2
	rm test test2
	./filter 200 1000 | sed '$$d' > test
	python filter.py test > test2
	diff test test2
	rm test test2
	./filter 1001 1001 | sed '$$d' > test
	python filter.py test > test2
	diff test test2
	rm test test2
