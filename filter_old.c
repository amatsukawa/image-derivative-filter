#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

typedef struct matrix Matrix;
struct matrix {
    int width;
    int height;
    int* data;
};

Matrix* allocateMatrix(int width, int height) {
    Matrix* m = (Matrix*) malloc(sizeof(Matrix));
    m->width = width;
    m->height = height;
    m->data = (int*) malloc(sizeof(int)*width*height);
    return m;
}

void freeMatrix(Matrix* m) {
    free(m->data);
    free(m);
}

void fillMatrix(Matrix* m) {
    int length = m->width * m->height;
    m->data = (int*) malloc(sizeof(int)*length);
    int* current = m->data;
    for (int i = 0; i < length; i++) {
        *current = rand();
        current++;
    }
}


void printMatrix(Matrix* m) {
    int* current = m->data;
    for (int i = 0; i < m->height; i++) {
        for (int j = 0; j < m->width; j++) {
            printf("%d ", *current);
            current++;
        }
        printf("\n");
    }
}

void naiveTranspose(Matrix* src, Matrix* dst) {
    for (int y = 0; y < src->height; y++) {
        for (int x = 0; x < src->width; x++) {
            dst->data[dst->width*x+y] = src->data[src->width*y+x];
        }
    }
}

void cacheObliviousTranspose(int x, int w, int y, int h,
        Matrix* src, Matrix* dst) {
    if ((w == 1) && (h==1)) {
        dst->data[dst->width*y+x] = src->data[src->width*x+y];
        return;
    }
    if (w >= h) {
        int wmid = w / 2;
        cacheObliviousTranspose(x, wmid, y, h, src, dst);
        cacheObliviousTranspose(x+wmid, w-wmid, y, h, src, dst);
        return;
    }
    if (w < h) {
        int hmid = h / 2;
        cacheObliviousTranspose(x, w, y, hmid, src, dst);
        cacheObliviousTranspose(x, w, y+hmid, h-hmid, src, dst);
        return;
    }
}

Matrix* transposeMatrix(Matrix* src, int alg) {
    Matrix* dst = allocateMatrix(src->height, src->width);
    if (alg == 0) {
        naiveTranspose(src, dst);
    } else {
        cacheObliviousTranspose(0, src->width, 0, src->height, src, dst);
    }
    return dst;
}

int maybeReplace(int val, int* pair) {
    if (val < pair[0]) {
        pair[0] = val;
    }
    if (val > pair[1]) {
        pair[1] = val;
    }
    return val;
}

Matrix* applyFilter(Matrix* src, int* pair) {
    Matrix* dst = allocateMatrix(src->width, src->height);
    for (int y = 0; y < src->height; y++) {
        int* srcRow = src->data + src->width*y;
        int* dstRow = dst->data + dst->width*y;
        dstRow[0] = maybeReplace(-srcRow[1],pair);
        for (int x = 1; x < src->width-1; x++) {
            dstRow[x] = maybeReplace(srcRow[x-1] - srcRow[x+1], pair);
        }
        dstRow[dst->width-1] = maybeReplace(srcRow[src->width-2], pair);
    }
    return dst;
}

int main(int argc, char* argv[]){
    if (argc < 3) {
        printf("Usage: ./filter width height [debug=1] [repeat=1] [transpose]\n");
        return 1;
    }
    int width = atoi(argv[1]);
    int height = atoi(argv[2]);
    int debug = 1;
    int repeat = 1;
    int transpose = 1;
    if (argc > 3) {
        debug = atoi(argv[3]);
    }
    if (argc > 4) {
        repeat = atoi(argv[4]);
    }
    if (argc > 5) {
        transpose = atoi(argv[5]);
    }
    if (transpose == 0)
        printf("Using naive transpose...\n");
    else
        printf("Using cache oblivious transpose...\n");
    double totalTime = 0.0;
    for (int i = 0; i < repeat; i++) {
        unsigned int start = clock();
        Matrix* mtx = allocateMatrix(width, height);
        srand(time(NULL));
        fillMatrix(mtx);

        int dxPair[2] = {INT_MAX, INT_MIN};
        int dyPair[2] = {INT_MAX, INT_MIN};

        Matrix* mtxDx = applyFilter(mtx, dxPair);
        Matrix* mtxT = transposeMatrix(mtx, transpose);
        Matrix* mtxDyT = applyFilter(mtxT, dyPair);
        Matrix* mtxDy = transposeMatrix(mtxDyT, transpose);
        double elapsed = ((double) (clock() - start) * 1000) / CLOCKS_PER_SEC;
        totalTime += elapsed;
        if (debug) {
            printf("Mtx\n");
            printMatrix(mtx);
            printf("\nDx\n");
            printMatrix(mtxDx);
            printf("\nDy\n");
            printMatrix(mtxDy);
        }
        printf("\n(min, max): dx=(%d, %d), dy=(%d, %d)\n",
            dxPair[0], dxPair[1], dyPair[0], dyPair[1]);
        printf("elapsed: %f msecs\n", elapsed);
        freeMatrix(mtx);
        freeMatrix(mtxDx);
        freeMatrix(mtxT);
        freeMatrix(mtxDyT);
        freeMatrix(mtxDy);
    }
    if (repeat > 1) {
        double avg = totalTime / repeat;
        printf("\navg elapsed: %f msecs\n", avg);

    }
    return 0;
}