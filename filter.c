#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include "matrix.h"


// applies the [-1, 0, 1] filter along the x axis, returns a new matrix
// that represents the x-direction derivative of src.
// takes two pointers to ints, and updates them with the min and max values
// of the derivative.
// we zero-extend our matrix, so indices out of bounds have value 0
// whe computing the filter.
Matrix* applyHorizontalFilter(Matrix* src, int* min, int* max) {
    Matrix* dst = allocateMatrix(src->width, src->height, src->blockSize);
    Block *prevSrc, *currSrc, *nextSrc, *currDst;
    for (int yBlock = 0; yBlock < src->blockHeight; yBlock++) {
        prevSrc = NULL;
        currSrc = src->blocks + yBlock * src->blockWidth;
        nextSrc = currSrc->right;
        currDst = dst->blocks + yBlock * dst->blockWidth;
        for (int xBlock = 0; xBlock < src->blockWidth; xBlock++) {
            for (int y = 0; y < currSrc->height; y++) {
                for (int x = 0; x < currSrc->width; x++) {
                    // compute prev
                    // if x = 0, we need to get it from the prev block, or set
                    // it to zero if no prev block. otherwise, get it from
                    // left of current index
                    int prev;
                    if (x == 0 && prevSrc == NULL)
                        prev = 0;
                    else if (x == 0)
                        prev = prevSrc->data[(y+1)*prevSrc->width-1];
                    else
                        prev = currSrc->data[y*currSrc->width+x-1];
                    // compute next
                    // if x = width-1, we need to get it from the next block, or set
                    // it to zero if no next block. otherwise, get it from
                    // right of current index
                    int next;
                    if (x == currSrc->width-1 && nextSrc == NULL)
                        next = 0;
                    else if (x == currSrc->width-1)
                        next = nextSrc->data[y*nextSrc->width];
                    else
                        next = currSrc->data[y*currSrc->width+x+1];
                    // the [-1, 0, 1] filter ends up being prev - next
                    int val = prev - next;
                    currDst->data[y*currDst->width+x] = val;
                    if (val < *min)
                        *min = val;
                    if (val > *max)
                        *max = val;
                }
            }
            prevSrc = currSrc;
            currSrc = nextSrc;
            if (currSrc != NULL)
                nextSrc = currSrc->right;
            currDst = currDst->right;
        }
    }
    return dst;
}

// similar to applyHorizontalFilter, except everything works vertically instead.
Matrix* applyVerticalFilter(Matrix* src, int* min, int* max) {
    Matrix* dst = allocateMatrix(src->width, src->height, src->blockSize);
    Block *prevSrc, *currSrc, *nextSrc, *currDst;
    for (int xBlock = 0; xBlock < src->blockWidth; xBlock++) {
        prevSrc = NULL;
        currSrc = src->blocks + xBlock;
        nextSrc = currSrc->down;
        currDst = dst->blocks + xBlock;
        for (int yBlock = 0; yBlock < src->blockHeight; yBlock++) {
            for (int y = 0; y < currSrc->height; y++) {
                for (int x = 0; x < currSrc->width; x++) {
                    // get prev.
                    // if there is no previous block, then we zero-extend
                    // if there is a previous block and y = 0, then we want
                    // the element from the last row of the prev block, at the
                    // same x location
                    // otherwise, we simply grab prev from our current block
                    int prev;
                    if (y == 0 && prevSrc == NULL)
                        prev = 0;
                    else if (y == 0)
                        prev = prevSrc->data[(prevSrc->height-1)*prevSrc->width+x];
                    else
                        prev = currSrc->data[(y-1)*currSrc->width+x];
                    // logic for next is similar to prev above
                    int next;
                    if (y == currSrc->height-1 && nextSrc == NULL)
                        next = 0;
                    else if (y == currSrc->height-1)
                        next = nextSrc->data[x];
                    else
                        next = currSrc->data[(y+1)*currSrc->width+x];
                    int val = prev - next;
                    currDst->data[y*currDst->width+x] = val;
                    if (val < *min)
                        *min = val;
                    if (val > *max)
                        *max = val;
                }
            }
            prevSrc = currSrc;
            currSrc = nextSrc;
            if (currSrc != NULL)
                nextSrc = currSrc->down;
            currDst = currDst->down;
        }
    }
    return dst;
}

int main(int argc, char* argv[]) {
    int BLOCKSIZE = 200;
    if (argc < 3) {
        printf("Usage: ./filter width height [debug=1] [repeat=1]\n");
        printf("debug flag sets whether to print out matrices. turn off for large mtx.\n");
        printf("repeat flag sets how many times to repeat the experiment.\n");
        return 1;
    }
    srand(time(NULL));
    int width = atoi(argv[1]);
    int height = atoi(argv[2]);
    int debug = 1;
    int repeat = 1;
    if (argc > 3) {
        debug = atoi(argv[3]);
    }
    if (argc > 4) {
        repeat = atoi(argv[4]);
    }
    double totalTime = 0.0;
    for (int r = 0; r < repeat; r++) {
        // uses clock cycles / cycle time to measure msecs
        unsigned int start = clock();
        Matrix* mtx = allocateMatrix(width, height, BLOCKSIZE);
        fillMatrix(mtx);
        int dxmin, dymin, dxmax, dymax;
        dxmin = dymin = INT_MAX;
        dxmax =dymax = INT_MIN;
        Matrix* mtxDx = applyHorizontalFilter(mtx, &dxmin, &dxmax);
        Matrix* mtxDy = applyVerticalFilter(mtx, &dymin, &dymax);
        double elapsed = ((double) (clock() - start) * 1000) / CLOCKS_PER_SEC;
        totalTime += elapsed;
        if (debug) {
            printf("Mtx\n");
            printMatrix(mtx);
            printf("Dx\n");
            printMatrix(mtxDx);
            printf("Dy\n");
            printMatrix(mtxDy);
        }
        printf("(min, max): dx=(%d, %d), dy=(%d, %d)\n",
            dxmin, dxmax, dymin, dymax);
        printf("elapsed: %f msecs\n", elapsed);
        freeMatrix(mtx);
        freeMatrix(mtxDx);
        freeMatrix(mtxDy);
    }
    if (repeat > 1) {
        double avg = totalTime / repeat;
        printf("\navg elapsed: %f msecs\n", avg);

    }
    return 0;
}